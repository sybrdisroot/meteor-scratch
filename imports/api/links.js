import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';

export default Links = new Mongo.Collection('links');

export function insertLink(title, url) {
    Links.insert({ title, url, createdAt: new Date() });
}

Meteor.methods({
    'links.insert'(title, url) {
        insertLink(title, url);
    }
});
