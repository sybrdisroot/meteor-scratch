import React, { useState, useEffect } from 'react';
import Hello from './Hello.jsx';
import Info from './Info.jsx';
import LinkForm from './LinkForm.jsx';
import AccountsUIWrapper from './AccountsUIWrapper.jsx';


function Example() {
  // Declare a new state variable, which we'll call "count"
  const [count, setCount] = useState(0);

   // Similar to componentDidMount and componentDidUpdate:
  useEffect(() => {
    // Update the document title using the browser API
    document.title = `You clicked ${count} times`;
  });

  return (
    <div>
      <p>You clicked {count} times</p>
      <button onClick={() => setCount(count + 1)}>
        Click me
      </button>
    </div>
  );
}

const App = () => (
  <div>
    <AccountsUIWrapper />
    <h1>Welcome to Meteor!</h1>
    <Hello />
    <Info />
    <hr />
    <Example />
    <hr />
    <LinkForm />
  </div>
);

export default App;
