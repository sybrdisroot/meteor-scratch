import { Meteor } from 'meteor/meteor';
import React, { useState } from 'react';

export default function LinkForm () {
    const 
        [linkTitle, setLinkTitle] = useState("Sense Títol"),
        [linkURL, setLinkURL] = useState("null")
    ;

    return (
      <div>
        <input type="text" onChange={(tit) => {
            //console.dir("tit: ", tit.target.value);
            setLinkTitle(tit.target.value)
        }} />
        <input type="text" onChange={(url) => setLinkURL(url.target.value)} />
        <button onClick={(ev) => {
            Meteor.call('links.insert', linkTitle, linkURL)
        }}>Crea l'enllaç
        </button>
      </div>
    );
}